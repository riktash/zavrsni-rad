package diplomski.rad.diplomskirad.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;
import diplomski.rad.diplomskirad.Activity.DetailsActivity;
import diplomski.rad.diplomskirad.Activity.MainActivity;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterMovies;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterTvShows;
import diplomski.rad.diplomskirad.Database.LocalDB;
import diplomski.rad.diplomskirad.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyWatchlistFragment extends android.support.v4.app.Fragment {

    ViewPager mViewPager;

    TabLayout tabLayout;

    SectionsPagerAdapter mSectionsPagerAdapter;

    public MyWatchlistFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_watchlist, container, false);

        mViewPager = view.findViewById(R.id.my_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = view.findViewById(R.id.my_tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSectionsPagerAdapter = new SectionsPagerAdapter(((MainActivity)getActivity()).fm());
    }

    public int tabPosition(){return tabLayout.getSelectedTabPosition();}

    public static class PlaceholderFragment extends android.support.v4.app.Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        private Disposable disposable;

        ArrayAdapterMovies arrayAdapterMovies;
        ArrayAdapterTvShows arrayAdapterTvShows;

        public PlaceholderFragment(){}

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ListView listView = rootView.findViewById(R.id.itemContainer);
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1){
                arrayAdapterMovies = new ArrayAdapterMovies(getContext(), R.layout.listview_item, new ArrayList<>(), false);
                listView.setAdapter(arrayAdapterMovies);
                getMovies();
            } else {
                arrayAdapterTvShows = new ArrayAdapterTvShows(getContext(), R.layout.listview_item, new ArrayList<>(), false);
                listView.setAdapter(arrayAdapterTvShows);
                getTvShows();
            }
            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                if(getArguments().getInt(ARG_SECTION_NUMBER) == 1){
                    intent.putExtra("Title", arrayAdapterMovies.getItem(position).getTitle());
                    intent.putExtra("Image", arrayAdapterMovies.getItem(position).getPoster_path());
                    intent.putExtra("Overview", arrayAdapterMovies.getItem(position).getOverview());
                } else {
                    intent.putExtra("Title", arrayAdapterTvShows.getItem(position).getName());
                    intent.putExtra("Image", arrayAdapterTvShows.getItem(position).getPoster_path());
                    intent.putExtra("Overview", arrayAdapterTvShows.getItem(position).getOverview());
                }
                startActivity(intent);
            });
            return rootView;
        }

        void getMovies(){
            disposable = LocalDB.getInstance().getAllMovies()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(movie -> arrayAdapterMovies.addItem(movie));
        }

        void getTvShows(){
            disposable = LocalDB.getInstance().getAllTvShows()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tvShow -> arrayAdapterTvShows.addItem(tvShow));
        }

        @Override
        public void onDestroyView() {
            if(disposable != null && !disposable.isDisposed())
                disposable.dispose();
            Log.d("Tu sam!!", "onDestroyView: ");
            super.onDestroyView();
        }
    }

    public static class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
