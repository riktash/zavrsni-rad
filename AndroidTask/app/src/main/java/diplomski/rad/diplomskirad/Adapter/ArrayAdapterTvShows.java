package diplomski.rad.diplomskirad.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import diplomski.rad.diplomskirad.Database.FirebaseDB;
import diplomski.rad.diplomskirad.Model.TvShow;
import diplomski.rad.diplomskirad.R;

public class ArrayAdapterTvShows extends ArrayAdapter<TvShow> {

    private Context context;
    private int resource;
    private ArrayList<TvShow> objects;
    private boolean add;

    public ArrayAdapterTvShows(@NonNull Context context, int resource, @NonNull ArrayList<TvShow> objects, Boolean add) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
        this.add = add;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(resource, parent, false);
        }
        ImageView posterImage = view.findViewById(R.id.posterImage);
        TextView title = view.findViewById(R.id.title);
        if(position > -1) {
            TvShow item = getItem(position);
            title.setText(item.getName());
            Picasso.get()
                    .load(context.getString(R.string.image_url) + item.getPoster_path())
                    .fit()
                    .centerInside()
                    .into(posterImage);
        }
        TextView addRemove = view.findViewById(R.id.add);
        if(!add) addRemove.setText(R.string.Remove);
        addRemove.setOnClickListener(v -> {
            if(add) {
                FirebaseDB.addTvShow(getItem(position));
                Toast.makeText(context, "Added to watchlist", Toast.LENGTH_SHORT).show();
            } else {
                FirebaseDB.removeTvShow(getItem(position), this);
            }
        });
        return view;
    }

    public void setItems(ArrayList<TvShow> objects){
        this.objects.clear();
        this.objects.addAll(objects);
        notifyDataSetChanged();
    }

    public void addItem(TvShow tvShow){
        if(!objects.contains(tvShow)) {
            this.objects.add(tvShow);
            notifyDataSetChanged();
        }
    }
}
