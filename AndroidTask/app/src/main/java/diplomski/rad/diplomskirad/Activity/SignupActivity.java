package diplomski.rad.diplomskirad.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import diplomski.rad.diplomskirad.R;

public class SignupActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    EditText inputEmail;
    EditText inputPassword;
    EditText inputPasswordAgain;
    AppCompatButton btnSignup;
    TextView linkLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();

        inputEmail = findViewById(R.id.input_email);
        inputPassword = findViewById(R.id.input_password);
        inputPasswordAgain = findViewById(R.id.input_password_again);
        btnSignup = findViewById(R.id.btn_signup);
        linkLogin = findViewById(R.id.link_login);

        btnSignup.setOnClickListener(v -> {
            if (inputPassword.getText().toString().equals(inputPasswordAgain.getText().toString())) {
                mAuth.createUserWithEmailAndPassword(inputEmail.getText().toString(), inputPassword.getText().toString())
                        .addOnCompleteListener(this, task -> {
                            if (task.isSuccessful()) {
                                setResult(1);
                                finish();
                            } else {
                                Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show();
            }
        });

        linkLogin.setOnClickListener(v -> {
            setResult(0);
            finish();
        });
    }
}
