package diplomski.rad.diplomskirad.Database;

import diplomski.rad.diplomskirad.Model.Movie;
import diplomski.rad.diplomskirad.Model.TvShow;
import io.reactivex.subjects.BehaviorSubject;

public class LocalDB {
    private BehaviorSubject<TvShow> tvShowsSubject;
    private BehaviorSubject<Movie> moviesSubject;

    private static LocalDB instance;

    private LocalDB(){
        tvShowsSubject = BehaviorSubject.create();
        moviesSubject = BehaviorSubject.create();
    }

    public static LocalDB getInstance(){
        if(instance == null)
            instance = new LocalDB();
        return instance;
    }

    void addTvShow(TvShow tvShow){
        tvShowsSubject.onNext(tvShow);
    }

    public io.reactivex.Observable<TvShow> getAllTvShows(){
        FirebaseDB.getAllTvShows(tvShowsSubject);
        return tvShowsSubject;
    }

    void addMovie(Movie movie){
        moviesSubject.onNext(movie);
    }

    public io.reactivex.Observable<Movie> getAllMovies(){
        FirebaseDB.getAllMovies(moviesSubject);
        return moviesSubject;
    }
}
