package diplomski.rad.diplomskirad.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterMovies;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterTvShows;
import diplomski.rad.diplomskirad.Model.ResultMovies;
import diplomski.rad.diplomskirad.Model.ResultTvShows;
import diplomski.rad.diplomskirad.R;
import diplomski.rad.diplomskirad.Retrofit.RetrofitClient;

public class SearchActivity extends AppCompatActivity {

    private ImageButton back;
    private ImageButton clear;
    private EditText text;
    private ListView listView;

    private ArrayAdapterMovies arrayAdapterMovies;
    private ArrayAdapterTvShows arrayAdapterTvShows;

    private BehaviorSubject behaviorSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        back = findViewById(R.id.back);
        clear = findViewById(R.id.cancel);
        text = findViewById(R.id.searchText);
        listView = findViewById(R.id.liveSearchList);

        final int movies = getIntent().getIntExtra("Movies", 0);

        if(movies == 0){
            arrayAdapterMovies = new ArrayAdapterMovies(this, R.layout.listview_item, new ArrayList<>(), true);
            listView.setAdapter(arrayAdapterMovies);
        } else {
            arrayAdapterTvShows = new ArrayAdapterTvShows(this, R.layout.listview_item, new ArrayList<>(), true);
            listView.setAdapter(arrayAdapterTvShows);
        }

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(SearchActivity.this, DetailsActivity.class);
            if(movies == 0){
                intent.putExtra("Title", arrayAdapterMovies.getItem(position).getTitle());
                intent.putExtra("Image", arrayAdapterMovies.getItem(position).getPoster_path());
                intent.putExtra("Overview", arrayAdapterMovies.getItem(position).getOverview());
            } else {
                intent.putExtra("Title", arrayAdapterTvShows.getItem(position).getName());
                intent.putExtra("Image", arrayAdapterTvShows.getItem(position).getPoster_path());
                intent.putExtra("Overview", arrayAdapterTvShows.getItem(position).getOverview());
            }
            startActivity(intent);
        });

        back.setOnClickListener(v -> finish());

        clear.setOnClickListener(v -> text.getText().clear());

        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    if (movies == 0)
                        searchMovies(s.toString());
                    else
                        searchTvShows(s.toString());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void searchMovies(String query) {
        if (behaviorSubject == null) {
            behaviorSubject = BehaviorSubject.create();
            behaviorSubject
                    .switchMap(searchResults -> RetrofitClient.getInstance().getMovies(searchResults.toString()))
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<ResultMovies>() {
                        @Override
                        public void onNext(ResultMovies resultMovies) {
                            arrayAdapterMovies.setItems(resultMovies.getResults());
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        behaviorSubject.onNext(query);
    }

    public void searchTvShows(String query) {
        if (behaviorSubject == null) {
            behaviorSubject = BehaviorSubject.create();
            behaviorSubject
                    .switchMap(searchValue -> RetrofitClient.getInstance().getTvShows(searchValue.toString()))
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<ResultTvShows>() {
                        @Override
                        public void onNext(ResultTvShows resultTvShows) {
                            arrayAdapterTvShows.setItems(resultTvShows.getResults());
                        }
                        @Override
                        public void onError(Throwable e) {

                        }
                        @Override
                        public void onComplete() {

                        }
                    });
        }
        behaviorSubject.onNext(query);
    }
}