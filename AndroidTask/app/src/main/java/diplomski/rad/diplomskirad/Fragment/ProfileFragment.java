package diplomski.rad.diplomskirad.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import de.hdodenhof.circleimageview.CircleImageView;
import diplomski.rad.diplomskirad.R;

public class ProfileFragment extends android.support.v4.app.Fragment {

    public ProfileFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        CircleImageView profilePic = view.findViewById(R.id.profile_pic_fragment);
        EditText name = view.findViewById(R.id.profile_name_fragment);
        EditText email = view.findViewById(R.id.profile_email_fragment);
        EditText password = view.findViewById(R.id.profile_password_fragment);

        name.setKeyListener(null);
        email.setKeyListener(null);
        password.setKeyListener(null);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        profilePic.setImageDrawable(getContext().getDrawable(R.drawable.ic_person_black_24dp));
        name.setText(user.getEmail());
        email.setText(user.getEmail());
        password.setText("********");

        return view;
    }

}
