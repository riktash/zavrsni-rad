package diplomski.rad.diplomskirad.Model;

import java.util.ArrayList;

public class ResultMovies {
    private ArrayList<Movie> results;

    public ArrayList<Movie> getResults() {
        return results;
    }

    public void setResults(ArrayList<Movie> results) {
        this.results = results;
    }
}
