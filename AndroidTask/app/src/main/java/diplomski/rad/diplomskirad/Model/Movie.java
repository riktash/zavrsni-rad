package diplomski.rad.diplomskirad.Model;

import io.realm.RealmObject;

public class Movie extends RealmObject{
    private int id;
    private String title;
    private String poster_path;
    private String overview;

    public Movie(int id, String title, String poster_path, String overview) {
        this.id = id;
        this.title = title;
        this.poster_path = poster_path;
        this.overview = overview;
    }

    public Movie() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getId() == ((Movie) obj).getId();
    }
}
