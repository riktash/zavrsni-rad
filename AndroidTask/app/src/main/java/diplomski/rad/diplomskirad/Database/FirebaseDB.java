package diplomski.rad.diplomskirad.Database;

import android.support.annotation.NonNull;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Map;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterMovies;
import diplomski.rad.diplomskirad.Adapter.ArrayAdapterTvShows;
import diplomski.rad.diplomskirad.Model.Movie;
import diplomski.rad.diplomskirad.Model.TvShow;
import io.reactivex.subjects.BehaviorSubject;

public class FirebaseDB {

    private static DatabaseReference instance;

    public static DatabaseReference getInstance() {
        if (instance == null) {
            instance = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Watchlist");
        }
        return instance;
    }

    public static void addTvShow(TvShow tvShow){
        FirebaseDB.getInstance().child("TvShows").child(Integer.toString(tvShow.getId())).child("name").setValue(tvShow.getName());
        FirebaseDB.getInstance().child("TvShows").child(Integer.toString(tvShow.getId())).child("overview").setValue(tvShow.getOverview());
        FirebaseDB.getInstance().child("TvShows").child(Integer.toString(tvShow.getId())).child("poster_path").setValue(tvShow.getPoster_path());
        LocalDB.getInstance().addTvShow(tvShow);
    }

    public static void addMovie(Movie movie){
        FirebaseDB.getInstance().child("Movies").child(Integer.toString(movie.getId())).child("name").setValue(movie.getTitle());
        FirebaseDB.getInstance().child("Movies").child(Integer.toString(movie.getId())).child("overview").setValue(movie.getOverview());
        FirebaseDB.getInstance().child("Movies").child(Integer.toString(movie.getId())).child("poster_path").setValue(movie.getPoster_path());
        LocalDB.getInstance().addMovie(movie);
    }

    public static void removeTvShow(TvShow tvShow, ArrayAdapterTvShows adapterTvShows){
        FirebaseDB.getInstance().child("TvShows").child(Integer.toString(tvShow.getId())).removeValue().addOnCompleteListener(task -> {
            adapterTvShows.remove(tvShow);
            adapterTvShows.notifyDataSetChanged();
            Toast.makeText(adapterTvShows.getContext(), "Removed from watchlist", Toast.LENGTH_SHORT).show();
        });
    }

    public static void removeMovie(Movie movie, ArrayAdapterMovies adapterMovies){
        FirebaseDB.getInstance().child("Movies").child(Integer.toString(movie.getId())).removeValue().addOnCompleteListener(task -> {
            adapterMovies.remove(movie);
            adapterMovies.notifyDataSetChanged();
            Toast.makeText(adapterMovies.getContext(), "Removed from watchlist", Toast.LENGTH_SHORT).show();
        });
    }

    static void getAllTvShows(BehaviorSubject behaviorSubject){
        FirebaseDB.getInstance().child("TvShows").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Map<String, Object>> tvShows = (Map<String, Map<String, Object>>) dataSnapshot.getValue();
                if(tvShows == null) return;
                for (Map.Entry<String, Map<String, Object>> tvShow : tvShows.entrySet()) {
                    TvShow temp = new TvShow();
                    temp.setId(Integer.parseInt(tvShow.getKey()));
                    for (Map.Entry<String, Object> tvShowData : tvShow.getValue().entrySet()) {
                        switch (tvShowData.getKey()) {
                            case "name":
                                temp.setName(tvShowData.getValue().toString());
                                break;
                            case "overview":
                                temp.setOverview(tvShowData.getValue().toString());
                                break;
                            case "poster_path":
                                temp.setPoster_path(tvShowData.getValue().toString());
                        }
                    }
                    behaviorSubject.onNext(temp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    static void getAllMovies(BehaviorSubject behaviorSubject){
        FirebaseDB.getInstance().child("Movies").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Map<String, Object>> movies = (Map<String, Map<String, Object>>) dataSnapshot.getValue();
                if(movies == null) return;
                for (Map.Entry<String, Map<String, Object>> movie : movies.entrySet()) {
                    Movie temp = new Movie();
                    temp.setId(Integer.parseInt(movie.getKey()));
                    for (Map.Entry<String, Object> movieData : movie.getValue().entrySet()) {
                        switch (movieData.getKey()) {
                            case "name":
                                temp.setTitle(movieData.getValue().toString());
                                break;
                            case "overview":
                                temp.setOverview(movieData.getValue().toString());
                                break;
                            case "poster_path":
                                temp.setPoster_path(movieData.getValue().toString());
                        }
                    }
                    behaviorSubject.onNext(temp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
