package diplomski.rad.diplomskirad.Model;

import java.util.ArrayList;

public class ResultTvShows {
    private ArrayList<TvShow> results;

    public ArrayList<TvShow> getResults() {
        return results;
    }

    public void setResults(ArrayList<TvShow> results) {
        this.results = results;
    }
}
