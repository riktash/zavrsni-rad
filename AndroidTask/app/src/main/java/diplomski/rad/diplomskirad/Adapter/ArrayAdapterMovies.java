package diplomski.rad.diplomskirad.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import diplomski.rad.diplomskirad.Database.FirebaseDB;
import diplomski.rad.diplomskirad.Fragment.MyWatchlistFragment;
import diplomski.rad.diplomskirad.Model.Movie;
import diplomski.rad.diplomskirad.Model.TvShow;
import diplomski.rad.diplomskirad.R;

public class ArrayAdapterMovies extends ArrayAdapter<Movie> {

    private Context context;
    private int resource;
    private ArrayList<Movie> objects;
    private boolean add;

    public ArrayAdapterMovies(@NonNull Context context, int resource, @NonNull ArrayList<Movie> objects, Boolean add) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
        this.add = add;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(resource, parent, false);
        }
        ImageView posterImage = view.findViewById(R.id.posterImage);
        TextView title = view.findViewById(R.id.title);
        if(position > -1) {
            Movie item = getItem(position);
            title.setText(item.getTitle());
            Picasso.get()
                    .load(context.getString(R.string.image_url) + item.getPoster_path())
                    .fit()
                    .centerInside()
                    .into(posterImage);
        }
        TextView addRemove = view.findViewById(R.id.add);
        if(!add) addRemove.setText(R.string.Remove);
        addRemove.setOnClickListener(v -> {
            if(add) {
                FirebaseDB.addMovie(getItem(position));
                Toast.makeText(context, "Added to watchlist", Toast.LENGTH_SHORT).show();
            } else {
                FirebaseDB.removeMovie(getItem(position), this);
            }
        });
        return view;
    }

    public void setItems(ArrayList<Movie> objects){
        this.objects.clear();
        this.objects.addAll(objects);
        notifyDataSetChanged();
    }

    public void addItem(Movie movie){
        if(!objects.contains(movie)) {
            this.objects.add(movie);
            notifyDataSetChanged();
        }
    }
}
