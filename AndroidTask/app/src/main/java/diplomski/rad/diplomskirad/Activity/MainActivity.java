package diplomski.rad.diplomskirad.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import java.util.concurrent.TimeUnit;
import de.hdodenhof.circleimageview.CircleImageView;
import diplomski.rad.diplomskirad.Fragment.HotFragment;
import diplomski.rad.diplomskirad.Fragment.MyWatchlistFragment;
import diplomski.rad.diplomskirad.Fragment.ProfileFragment;
import diplomski.rad.diplomskirad.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private HotFragment hotFragment;
    private MyWatchlistFragment myWatchlistFragment;
    private android.support.v4.app.Fragment currentFragment;
    private ProfileFragment profileFragment;

    private DrawerLayout mDrawerLayout;

    private Toolbar toolbar;

    private FirebaseAuth mAuth;

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(this);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View mNavigationHeader = navigationView.inflateHeaderView(R.layout.menu_header);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        navigateDrawerLayout(navigationView);

        CircleImageView profile_pic = mNavigationHeader.findViewById(R.id.profile_pic);
        TextView profile_name = mNavigationHeader.findViewById(R.id.profile_name);
        profile_name.setText(user.getEmail());

        fragmentManager = getSupportFragmentManager();
        hotFragment = new HotFragment();
        currentFragment = hotFragment;

        fragmentManager.beginTransaction().add(R.id.frame, hotFragment).commit();
    }

    public FragmentManager fm(){return getSupportFragmentManager();}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_search:
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("Movies", hotFragment.tabPosition());
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigateDrawerLayout(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(item -> {
            item.setChecked(true);
            mDrawerLayout.closeDrawers();

            switch (item.getItemId()) {
                case R.id.nav_profile:
                    if(profileFragment == null) {
                        profileFragment = new ProfileFragment();
                        fragmentManager.beginTransaction().add(R.id.frame, profileFragment).commit();
                    }
                    showFragment(profileFragment);
                    return true;
                case R.id.nav_watchlist:
                    if(myWatchlistFragment == null) {
                        myWatchlistFragment = new MyWatchlistFragment();
                        fragmentManager.beginTransaction().add(R.id.frame, myWatchlistFragment).commit();
                    }
                    showFragment(myWatchlistFragment);
                    return true;
                case R.id.nav_hot:
                    showFragment(hotFragment);
                    return true;
                case R.id.nav_sign_out:
                    Observable.timer(300, TimeUnit.MILLISECONDS)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new DisposableObserver<Long>() {
                                @Override
                                public void onNext(Long aLong) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onComplete() {
                                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                    mAuth.signOut();
                                    startActivity(intent);
                                    finish();
                                }
                            });
                default:
                    return super.onOptionsItemSelected(item);
            }
        });
    }

    private void showFragment(android.support.v4.app.Fragment newFragment){
        if(currentFragment == newFragment) return;
        fragmentManager.beginTransaction().hide(currentFragment).show(newFragment).commit();
        currentFragment = newFragment;
        toolbar.getMenu().getItem(0).setVisible(currentFragment.getClass() != ProfileFragment.class);
    }
}
